Repository hosting Math/SomeUtils, shared package by Erasmus, Urania and Castelao.

This is submoduled in the three projects above. Any changes to Math/SomeUtils should be done to this repository.
